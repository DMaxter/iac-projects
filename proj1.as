stack	EQU 	FDFFh		; Endereço para inicializar o stack
endOut	EQU 	FFFEh		; Endereço para output
endCur	EQU 	FFFCh		; Endereço para o porto de controlo
num 	EQU 	1010100101011011b	; Número "random" para gerar a sequencia
mask	EQU 	1000000000010110b	; Máscara para gerar a sequencia
zero	EQU 	48		; Caracter ASCII do digito 0
cNL	EQU 	7Dh		; Caracter ASCII para '}'
cSTOP	EQU 	7Bh		; Caracter ASCII para '{'
cTAB	EQU 	40h		; Caracter ASCII para '@'
ntent     EQU 	12

	ORIG	8000h
; '{' - Caracter terminador para strings
; '}' - Caracter newline para strings
; '@' - Caracter tab para strings
titulo	STR	'@@@@MasterMind}}Defina R2 para jogar}}{'
string	STR	'Tentativa(s) Restante(s): {'
win	STR	'}}}Parabens!}{'
lose	STR	'}}}Mais sorte para a proxima.}A sequencia era {'
again	STR	'Para jogar de novo, definir qualquer valor em R5}}}}}{'
valX	WORD	'X'
valO 	WORD	'O'
valT	WORD	'-'

; *******************************************************************************************************
; USADOS: R1, R2, R3, R4, R5, R6
;
; Início do programa
;
; R1 -> Sequencia
; R2 -> Tentativa
; R3 -> Semelhanças entre R1 e R2
; R4 -> Tentativas restantes
; R5 -> Saber se acertou ou não
; R6 -> Posição do cursor
; *******************************************************************************************************

	ORIG	0000h
	MOV 	R7, stack		; Inicialização do stack
	MOV 	SP, R7
novo:	MOV 	R7, FFFFh		; Inicialização do cursor
	MOV 	M[endCur], R7
	MOV 	R6, 0000h		; Cursor a zeros
 	PUSH	R6
	PUSH	titulo
	CALL	printSTR
	POP	R6
	MOV 	R4, ntent		; Inicializar o numero de tentativas restantes
	PUSH	R0		; Guardar espaço para a sequencia
	CALL	random		; Função que cria uma sequencia
	POP	R1		; Sequencia -> R1
inicio:	CMP	R2, R0
	BR.Z	inicio
	PUSH	R6
	PUSH 	R2
	CALL	printJog		; Mostra a jogada no ecrã
	POP	R2
	POP	R6
	DEC 	R4		; Reduz-se o numero de tentativas restantes
	PUSH	R0		; Guardar espaço para a semelhança
	PUSH	R1
	PUSH	R2
	CALL	compara		; Função que devolve a comparação
	POP	R2
	POP 	R1
	POP	R3		; Armazena-se a comparação em R3
	PUSH	R6
	PUSH	R0		; Espaço para saber se acertou
	PUSH	R3
	CALL	output		; Escreve a semelhança
	POP 	R3
	POP	R5
	POP	R6
	MOV 	R2, R0
	CMP	R5, 4
	MOV 	R5, R0
	JMP.Z	ganhou
	CMP	R4, R0		; Ve se as tentativas estão esgotadas
	JMP.Z	perdeu
	PUSH	R6
	PUSH	R4
	CALL	prntTent		; Escreve o numero de tentativas restantes
	POP	R4
	POP	R6
	PUSH	R6
	CALL	newline
	POP	R6
	JMP 	inicio

; *******************************************************************************************************
; USADOS: R1, R2, R3, R4, R5, R6, R7
;
; Faz o output dos resultados
;
; R1 -> Numero de caracteres a contar
; R2 -> Comparação
; R3 -> 4 ultimos bits de R2
; R4 -> Numero de X a escrever
; R5 -> Numero de O a escrever
; R6 -> Numero de - a escrever
; R7 -> Posição do cursor
; *******************************************************************************************************
; Código:	A - Algarismo com correspondente
;	B - Algarismo (da tentativa) já com correspondente
;	C - Sitio certo
;	D - Sitio errado
; *******************************************************************************************************

output:	PUSH	R1
	PUSH	R2
	PUSH	R3
	PUSH 	R4
	PUSH	R5
	PUSH	R6
	PUSH	R7
	MOV 	R4, R0		; Contadores a zero
	MOV 	R5, R0
	MOV 	R6, R0
	MOV 	R1, 4		; Número de caracteres a escrever
	MOV 	R2, M[SP+9]	; R2 fica com o valor da comparação
	MOV 	R7, M[SP+11]
compInic:	MOV 	R3, R2		; Copiar R2 para R3
	AND	R3, Fh
	CMP	R3, Ch
	BR.Z	contaX
	CMP	R3, 0h
	BR.Z	contaT
	INC 	R5
	DEC	R1
compFin:	SHR	R2, 4
	CMP	R1, R0
	BR.NZ	compInic
	MOV 	R1, 4
	BR	escreve
contaT:	INC	R6
	DEC	R1
	BR	compFin
contaX:	INC 	R4
	DEC	R1
	BR	compFin
escreve:	MOV 	M[SP+9], R0
	MOV 	M[SP+10], R4
cmpX:	CMP	R4, R0		; Ver se existem caracteres X a escrever
	BR.NZ	escreveX
cmpO:	CMP	R5, R0		; Ver se existem caracteres O a escrever
	BR.NZ	escreveO
cmpT:	CMP	R6, R0		; Ver se existem caracteres - a escrever
	BR.NZ	escreveT
	BR	escrFim
escreveX:	MOV 	R1, M[valX]
	MOV 	M[endCur], R7
	INC	R7
	MOV 	M[endOut], R1
	DEC	R4
	BR	cmpX
escreveO: MOV 	R1, M[valO]
	MOV 	M[endCur], R7
	INC	R7
	MOV 	M[endOut], R1
	DEC 	R5
	BR	cmpO
escreveT: MOV 	R1, M[valT]
	MOV 	M[endCur], R7
	INC	R7
 	MOV 	M[endOut], R1
	DEC	R6
	BR	cmpT
escrFim:	PUSH	R7
	CALL	newline
	POP	R7
	MOV 	M[SP+11], R7
	POP	R7
	POP	R6
	POP	R5
	POP 	R4
	POP	R3
	POP	R2
	POP	R1
	RET

; *******************************************************************************************************
; USADOS: R1, R2, R3, R4
;
; Coloca a jogada e tentativas restantes no ecrã
;
; R1 -> Tentativa
; R2 -> 4 primeiros bits de R1
; R3 -> Divisor (10)
; R4 -> Posição do cursor
; *******************************************************************************************************

printJog:	PUSH	R1
	PUSH	R3
	PUSH	R4
	MOV 	R1, M[SP+5]
	MOV 	R4, M[SP+6]
printIni:	MOV 	R2, R1		; R2 está na pilha logo não é preciso guardar o seu valor
	AND	R2, F000h		; 4 primeiros bits de R1
	SHR	R2, 12		; Para ficarmos com o digito no início
	ADD 	R2, zero		; Soma-se o valor do caracter 0 para se conseguir o código ASCII do número
	MOV 	M[endCur], R4
	INC	R4
	MOV 	M[endOut], R2
	SHL	R1, 4		; Deslocar R1 4 bits para a esquerda
	CMP	R1, R0
	BR.NZ	printIni
	PUSH	R4
	CALL 	newline
	POP	R4
	MOV 	M[SP+6], R4
	POP	R4
	POP	R3
	POP	R1
	RET

; *******************************************************************************************************
; USADOS: R1, R2, R3
;
; Coloca no ecrã o número de tentativas restantes
;
; R1 -> Endereço do primeiro elemento da string
; R2 -> Divisor (10) usado para fazer print de letras
; R3 -> Posição do cursor
; *******************************************************************************************************

prntTent:	PUSH	R1
	PUSH	R2
	PUSH	R3
	MOV 	R3, M[SP+6]
	PUSH	R3
	PUSH	string
	CALL	printSTR		; Escreve string
	POP	R3
	MOV 	R1, M[SP+5]
repTent:	MOV 	R2, 10
	DIV	R1, R2
	ADD 	R1, zero
	ADD 	R2, zero
	CMP	R1, R0
	BR.Z	3
	MOV 	M[endCur], R3
	INC	R3
	MOV 	M[endOut], R1
	MOV 	M[endCur], R3
	INC	R3
	MOV 	M[endOut], R2
	PUSH	R3
	CALL	newline
	POP	R3
	MOV 	M[SP+6], R3
	POP	R3
	POP	R2
	POP	R1
	RET

; *******************************************************************************************************
; USADOS:	R1, R2, R3
;
; Coloca no ecrã uma string
;
; R1 -> Endereço do primeiro elemento da string
; R2 -> Valor contido em R1
; R3 -> Cursor
; *******************************************************************************************************

printSTR:	PUSH	R1
	PUSH	R2
	PUSH	R3
	MOV 	R1, M[SP+5]	; R1 fica com o endereço do 1º elemento da string
	MOV 	R3, M[SP+6]	; R3 fica com o cursor
prntloop:	MOV 	R2, M[R1]
	CMP	R2, cSTOP
	BR.Z	printFim
	CMP	R2, cNL
	BR.Z	printNL
	CMP	R2, cTAB
	BR.Z	printTAB
	MOV 	M[endCur], R3
	INC	R3
	MOV 	M[endOut], R2
prtCont:	INC	R1
	BR	prntloop
printFim:	MOV 	M[SP+6], R3
	POP	R3
	POP	R2
	POP	R1
	RETN	1
printNL:	PUSH	R3
	CALL	newline
	POP	R3
	BR	prtCont
printTAB:	ADD 	R3, 9h
	BR	prtCont

; *******************************************************************************************************
; USADOS: R1
;
; Coloca uma nova linha no ecrã
; *******************************************************************************************************

newline:	PUSH	R1
	MOV 	R1, M[SP+3]
	ADD 	R1, 100h		; Passar para a linha de baixo
	AND	R1, FF00h		; Colocar na primeira coluna
	MOV 	M[SP+3], R1
	POP	R1
	RET

; *******************************************************************************************************
; USADOS: R1, R2, R3, R4, R5
;
; Cria uma sequencia aleatória até ser definido R2
; *******************************************************************************************************
random:	PUSH 	R3
	PUSH 	R4
	MOV 	R1, num
	MOV 	R5, R0
teste:	CMP	R2, R0
	BR.NZ	divisao
	TEST 	R1, 1
	BR.Z	salto
	XOR	R1, mask
	ROR	R1, 1
	BR 	teste
salto:	ROR	R1, 1
	BR	teste

divisao:	MOV 	R3, R1
	MOV 	R4, 6
	AND	R3, Fh
	DIV	R3, R4
	INC	R4
	SHL	M[SP+4], 4
	ADD 	M[SP+4], R4
	SHR 	R1, 4
	CMP	R1, R0
	BR.NZ	divisao
	POP	R4
	POP	R3
	RET

; *******************************************************************************************************
; USADOS: R1, R2
;
; Compara R1 com R2
;
; R1 -> Sequencia
; R2 -> Tentativa
; *******************************************************************************************************
; Os valores de R1 e R2 não são salvaguardados agora porque já estão na pilha
compara:	MOV 	R1, M[SP+3]	; R1 -> Sequencia
	MOV 	R2, M[SP+2]	; R2 -> Tentativa
	CMP 	R1, R2		; Verificar se são iguais
	BR.NZ	rotacoes
	MOV 	R1, CCCCh
	MOV 	M[SP+4], R1	; Armazenar "R3" na pilha
	RET

; *******************************************************************************************************
; USADOS: R1, R2, R3
;
; ROTAÇÕES PARA CONTAR NUMERO DE X E DE O A COLOCAR NO ECRÃ
;
; R1 -> Sequencia
; R2 -> Tentativa
; R3 -> Ultimos 4 bits de R1		(temporário)
; R4 -> Ultimos 4 bits de R2		(temporário)
; R5 -> Número de X ou O a colocar	(contador)
; R6 -> Número de vezes que se usou	(contador) Em rotO para contar usos de R2
; R7 -> Número de vezes que se usou 	(contador) Só para rotO e para contar usos de R1
; *******************************************************************************************************
; Código:	A - Algarismo com correspondente
;	B - Algarismo (da tentativa) já com correspondente
;	C - Sitio certo
;	D - Sitio errado
; *******************************************************************************************************

rotacoes:	PUSH	R3		; Guardar os valores dos registos
	PUSH	R4
	PUSH	R5
	PUSH	R6
	MOV 	R5, R0		; Colocar os contadores a zero
	MOV 	R6, R0

; Conta o numero de X a colocar

rotX:	MOV 	R3, R1		; Copiar as sequencias para os temporários
	MOV 	R4, R2
	AND 	R3, Fh		; Ficar com os 4 ultimos bits nos temporários
	AND	R4, Fh
	CMP	R3, R4		; Se forem iguais salta
	BR.Z	incX
continX:	ROR	R1, 4
	ROR	R2, 4
	INC	R6
	CMP 	R6, 4
	BR.Z	passaX
	BR	rotX
incX:	INC	R5		; Incrementar o numero de 'X' a colocar
	AND 	R1, FFF0h		; Atribuição do código
	ADD 	R1, Ah
	AND	R2, FFF0h
	ADD 	R2, Bh
	BR	continX

; Passa o numero de 'X' para a pilha, em código

passaX:	CMP	R5, R0
	BR.Z	rotO
	SHL	M[SP+8], 4
	MOV 	R7, Ch
	ADD 	M[SP+8], R7
	DEC	R5
	BR	passaX

; Conta o numero de O a colocar

rotO:	MOV 	R3, R1		; Copiar as sequencias para os temporários
	MOV 	R4, R2
	MOV 	R6, R0		; Colocar os contadores a zero
	MOV 	R7, R0
	AND 	R3, Fh		; Ficar com os 4 ultimos bits nos temporários
; Roda R2

rotR2:	CMP	R3, Ah		; Se o algarismo já tiver um algarismo correspondente na tentativa salta
	BR.Z	rotR1		; E R1 é rodado
	CMP	R2, BBBBh		; Se já tiver sido tudo verificado salta para o fim de rotacoes
	JMP.Z	passaO		; Não é necessário comparar também R1 com AAAAh, pois o resultado é o mesmo
	AND	R4, Fh
	CMP	R3, R4		; Se forem iguais salta
	BR.Z	incO
	ROR	R2, 4		; Rodar R2 4 bits para a direita
	INC	R6		; Incrementar o numero de vezes que se usou R2
contO:	CMP	R6, 4		; Se tiver usado 4 vezes R2 salta para rodar R1
	BR.Z	rotR1
	MOV 	R4, R2
	BR	rotR2
incO:	INC	R5		; Incrementar o numero de 'O' a colocar
	AND	R1, FFF0h		; Atribuição do código
	ADD 	R1, Ah
	AND	R2, FFF0h
	ADD 	R2, Bh
	BR	contO

; Roda R1

rotR1:	INC 	R7		; Incrementar o numero de vezes que
	CMP	R7, 4		; Se tiver usado 4 vezes R1 salta para o fim de rotacoes
	BR.Z	passaO
	ROR	R1, 4		; Rodar R1 4 bits para a direita
	MOV 	R6, 0
	MOV 	R4, R2
	MOV 	R3, R1
	AND	R3, Fh
	JMP	rotR2

; Passa o numero de 'O' para a pilha, em código

passaO:	CMP	R5, R0
	BR.Z	rotFim
	SHL	M[SP+8], 4
	MOV 	R7, Dh		; Não é necessário PUSH porque o valor de R7 é agora descartável
	ADD 	M[SP+8], R7
	DEC	R5
	BR	passaO

rotFim:	POP	R6
	POP	R5
	POP	R4
	POP	R3
	RET

; *******************************************************************************************************
perdeu:	PUSH	R6
	PUSH	lose
	CALL	printSTR
	POP	R6
	PUSH	R6
	PUSH	R1
	CALL	printJog
	POP	R1
	POP	R6
	BR	pergunta

ganhou:	PUSH	R6
	PUSH	win
	CALL	printSTR
	POP 	R6
pergunta:	PUSH	R6
	PUSH	again
	CALL	printSTR
	POP	R6
fim:	CMP 	R5, R0
	JMP.NZ	novo
	BR	fim
