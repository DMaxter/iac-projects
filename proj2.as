; ALTERACAO NA LINHA 344 (NESTE FICHEIRO E 339 NA SUBMISSAO)
; A ROTINA DE LIMPAR A JANELA ENTRAVA NUM CICLO INFINITO,
; QUANDO O LIMITE ESTAVA DEFINIDO PARA 1750h
; DEVIDO A UMA INTRUCAO ANTERIOR QUE FAZIA SALTAR PARA O INICIO DA ROTINA
; RESOLVEU-SE ALTERANDO ESTE VALOR PARA 1800h 
; ====================================================================================
;			CONSTANTES
; Enderecos
; ====================================================================================
MASK_ADR	EQU 	FFFAh		; Endereco da mascara de interrupcoes
UNI_CONT	EQU 	FFF6h		; Endereco para as unidades de 100ms
TMP_CTRL 	EQU 	FFF7h		; Endereco para controlo do temporizador
DISPLAY3	EQU 	FFF3h		; Endereco para display 3
LCD_CONTR	EQU 	FFF4h		; Endereco para controlo do LCD
LCD_ESCR	EQU 	FFF5h		; Endereco para escrita no LCD
LEDS 	EQU 	FFF8h		; Endereco para controlo dos LEDS
STACK_ADR	EQU 	FDFFh 		; Endereco para iniciar o Stack Pointer
ESCR_ADR	EQU 	FFFEh		; Endereco para escrita na janela de texto
CUR_CTRL	EQU 	FFFCh		; Endereco para posicionar o cursor na janela de texto

; ====================================================================================
; Mascaras
; ====================================================================================
INT_MASK EQU 	1000010001111110b	; Mascara de interrupcoes
RAND_MASK EQU 	1110100110010010b 	; Mascara para random

; ====================================================================================
; Caracteres para strings
; ====================================================================================
NEWLINE 	EQU 	'}'		; Caracter para newline
END_STR	EQU 	'{'		; Caracter para fim de string
TABUL 	EQU 	'@'		; Caracter para tabulacao
CENTER 	EQU 	'~'		; Caracter para centrar texto
V_CENTER	EQU 	'^'		; Caracter para centrar verticalmente
CHR_CLEAR	EQU 	' '		; Caracter para limpar o ecra

; ====================================================================================
; Codificacao
; ====================================================================================
COD_X	EQU 	10b		; Codificacao para o X
COD_O	EQU 	1b		; Codificacao para o O
COD_T	EQU 	0b		; Codificacao para o -
VOID_SEQ	EQU 	000b		; Codificacao para elemento analisado na Sequencia
VOID_TENT	EQU 	111b		; Codificacao para elemento analisado na Tentativa

; ====================================================================================
; Variado
; ====================================================================================
N100MS	EQU 	5		; Numero de 100ms para realizar uma jogada
N_LINES	EQU 	24		; Numero de linhas
N_COLUMNS	EQU 	80 		; Numero de colunas
N_TENT	EQU 	12		; Numero de tentativas
NCHRTAB	EQU 	10		; Numero de caracteres para saltar na tabulacao
LCD_ON	EQU 	8000h		; Coloca o cursor na posicao inicial, com LCD ligado
LCD_CLEAR EQU 	8020h		; Limpa o ecra do LCD
LED_RESET EQU 	1111111111111111b	; Acende todos os LEDs
CUR_INI 	EQU 	FFFFh		; Posicao para iniciar o cursor da janela de texto

; ====================================================================================
; 			MEMORIA
; Sequencias
; ====================================================================================
	ORIG 	8000h
sequencia	WORD 	0		; Armazena a sequencia aleatoria
tentativa	WORD 	0		; Armazena a tentativa
semelhnca	WORD 	0		; Armazena a semelhanca
pontuacao WORD 	0		; Armazena a pontuacao atual
pont_out	WORD 	0		; Armazena a "pontuacao" para output
mel_pont	WORD	0		; Armazena a melhor pontuacao
leds	WORD	1111111111111111b	; Armazena o atual estado dos LEDs

; ====================================================================================
; Variaveis de estado
; ====================================================================================
espera 	WORD	1		; Variavel de estado para verificar se sai da rotina "random"
result 	WORD 	0		; Variavel para saber qual o resultado (0 - nao terminou, 1 - perdeu, 2 - ganhou)
n_inter	WORD	4		; Variavel para saber quantas vezes falta pressionar os botoes de pressao
primeira	WORD 	1		; Variavel de estado para saber se e a primeira jogada em que ganha

; ====================================================================================
; Cursor
; ====================================================================================
cur_texto	WORD 	0000h		; Armazena o cursor da janela de texto

; ====================================================================================
; Strings especiais
; ====================================================================================
dicas 	STR 	'-OX'		; String que contem o feedback da tentativa
nums	STR 	'0123456789'	; String que contem os numeros

; ====================================================================================
; Strings
; ====================================================================================
; Strings para escrever o logotipo
logo 	STR 	'^',10,'~ ______                                   ______   _             _ }'
logo1	STR	'~|  ___ \               _                 |  ___ \ (_)           | |}'
logo2 	STR 	'~| | _ | |  ____   ___ | |_   ____   ____ | | _ | | _  ____    __| |}'
logo3 	STR	'~| || || | / _  | /___)|  _) / _  ) / ___)| || || || ||  _ \  / _  |}'
logo4	STR 	'~| || || |( (_| ||___ || |__( (/ / | |    | || || || || | | |( (_| |}'
logo5 	STR 	'~|_||_||_| \____|(___/  \___)\____)|_|    |_||_||_||_||_| |_| \____|}}{'
inicio 	STR 	'}}}~Carregue no botao IA para comecar}{'	; String inicial
jogada	STR 	'^',14,'~JOGADA@@SEQUENCIA@@RESULTADO}}{'	; String das jogadas
msg_LCD 	STR 	'Melhor:{'				; String para LCD
perdeu 	STR 	'~Perdeu. A sequencia correta era     {'	; String para "perdeu"
ganhou 	STR 	'~Parabens! Acertou na sequencia correta}{'	; String para "ganhou"
game_over	STR 	'~Fim do Jogo}~Carregue em IA para recomecar}{'	; String final

; ====================================================================================
; 			PROGRAMA
; ====================================================================================
	ORIG 	0000h
	MOV 	R1, STACK_ADR		; Inicializacoes
	MOV 	SP, R1			; Pilha
	MOV 	R1, LCD_ON		; Ligar LCD
	MOV 	M[LCD_CONTR], R1
	MOV 	R1, CUR_INI		; Cursor da janela de texto
	MOV 	M[CUR_CTRL], R1
	MOV 	R1, INT_MASK		; Mascara de interrupcoes
	MOV 	M[MASK_ADR], R1
	PUSH	logo			; Coloca o "logo" no ecra
	PUSH 	M[cur_texto]
	CALL 	prntTxt
	POP 	M[cur_texto]
	PUSH 	inicio			; Coloca no ecra a mensagem de inicio
	PUSH 	M[cur_texto]
	CALL 	prntTxt
	POP 	M[cur_texto]
	PUSH 	msg_LCD			; Faz print para o LCD
	CALL 	prntLCD
	ENI
deNovo:	MOV 	R1, INTAF			; Ativa a interrupcao IA
	MOV 	M[FE0Ah], R1
	CALL 	random			; Gera a sequencia aleatoria
	MOV 	R1, nada			; Desativa a interrupcao IA
	MOV 	M[FE0Ah], R1
	MOV 	R1, INT1F 		; Ativa as interrupcoes
	MOV 	M[FE01h], R1
	MOV 	R1, INT2F
	MOV 	M[FE02h], R1
	MOV 	R1, INT3F
	MOV 	M[FE03h], R1
	MOV 	R1, INT4F
	MOV 	M[FE04h], R1
	MOV 	R1, INT5F
	MOV 	M[FE05h], R1
	MOV 	R1, INT6F
	MOV 	M[FE06h], R1
	MOV 	R1, INTCF			; Temporizador
	MOV 	M[FE0Fh], R1
	MOV 	R1, N100MS
	MOV 	M[UNI_CONT], R1
	MOV 	R1, 1
	MOV 	M[TMP_CTRL], R1
	CALL 	clrScrn			; Limpa a janela de texto
	PUSH 	jogada			; Escreve a "tabela"
	PUSH 	M[cur_texto]
	CALL 	prntTxt
	POP 	M[cur_texto]
ciclo:	MOV 	R1, M[pont_out]		; Coloca a "pontuacao" em R1
	MOV 	R2, DISPLAY3		; R2 fica com o endereco do display mais a esquerda
	MOV 	R3, 4			; Coloca o contador a 4
dspLoop:	MOV 	M[R2], R1			; Coloca a pontucao nos displays numericos
	SHR 	R1, 4
	DEC 	R2
	DEC 	R3
	CMP 	R3, R0			; Se ja colocou todos os digitos nos displays
	BR.NZ	dspLoop			; Sai do ciclo
	MOV 	R1, M[leds]		; R1 fica com o estado dos leds
	MOV 	M[LEDS], R1		; Atualiza os LEDs
	CMP 	R1, R0			; Se os segundos se tiverem esgotado
	JMP.Z 	tEsgt
	CMP 	M[n_inter], R0		; Se foram introduzidas as 4 interrupcoes
	BR.Z	digEsgt
	MOV 	R4, N_TENT
	CMP 	M[pontuacao], R4		; Se as tentativas estiverem esgotadas
	BR.Z	tEsgt			; Salta para fora do ciclo
	MOV 	R1, M[result]
	CMP 	R1, R0			; Verifica se o jogo acabou
	JMP.NZ	fim
	JMP	ciclo

digEsgt:	PUSH 	M[tentativa]		; Se tiverem ocorrido 4 interrupcoes
	PUSH 	M[sequencia]		; Compara a tentativa do jogador com a sequencia
	CALL	compara
	MOV 	R4, LED_RESET		; Faz reset aos LEDs
	MOV 	M[leds], R4
	PUSH 	M[pontuacao]		; Transforma a pontuacao em decimal
	PUSH 	4
	CALL 	transDec
	POP 	M[pont_out]
	JMP 	ciclo

tEsgt:	INC 	M[result]			; Coloca a variavel de estado a um
fim:	MOV 	M[TMP_CTRL], R0		; Para o temporizador
	MOV 	R1, nada			; Interrupcoes desativadas
	MOV 	M[FE01h], R1
	MOV 	M[FE02h], R1
	MOV 	M[FE04h], R1
	MOV 	M[FE05h], R1
	MOV 	M[FE06h], R1
	MOV 	M[FE0Fh], R1
	MOV 	R1, 4			; Coloca o numero de interrupcoes a receber
	MOV 	M[n_inter], R1 		; a 4
	CALL	clrScrn			; Limpa o ecra
	PUSH 	logo			; Coloca o "logo" no ecra
	PUSH 	M[cur_texto]
	CALL 	prntTxt
	POP 	M[cur_texto]
	MOV 	R1, M[result]		; Analisa o resultado
	CMP 	R1, 1
	JMP.Z	perde
	JMP.P	ganha
fimCont:	MOV 	M[primeira], R0		; Coloca a variavel de estado a zero
	PUSH 	M[cur_texto]		; Coloca uma nova linha
	CALL 	newline
	POP 	R1
	PUSH 	game_over			; Coloca a mensagem de fim do jogo
	PUSH 	R1
	CALL 	prntTxt
	POP 	M[cur_texto]
	MOV 	R1, 1			; Coloca o programa a espera da interrupcao IA
	MOV 	M[espera], R1
	MOV 	M[tentativa], R0		; Coloca a tentativa a zero
	MOV 	M[result], R0		; Coloca o resultado a zero
	MOV 	M[pontuacao], R0		; Coloca a pontuacao a zero
	MOV 	M[pont_out], R0		; Coloca o output da pontuacao a zero
	JMP 	deNovo

mudaPont:	MOV 	M[mel_pont], R1		; Atualiza a melhor pontuacao
	PUSH 	msg_LCD			; Faz print para o LCD
	CALL 	prntLCD
	JMP 	fimCont

perde:	PUSH 	perdeu			; Faz print da string de "perdeu"
	PUSH 	M[cur_texto]
	CALL 	prntTxt
	POP 	R1
	SUB 	R1, 4			; Posiciona corretamente o cursor
	PUSH 	M[sequencia]		; Faz print da sequencia
	PUSH 	R1
	CALL 	prtSeq
	POP 	R1
	PUSH	R1
	CALL 	newline
	POP 	M[cur_texto]
	JMP 	fimCont

ganha: 	PUSH 	ganhou			; Faz print da string de "ganhou"
	PUSH 	M[cur_texto]
	CALL 	prntTxt
	POP 	M[cur_texto]
	MOV 	R1, M[pontuacao]
	CMP 	M[primeira], R0
	JMP.NZ	mudaPont
	CMP 	R1, M[mel_pont]		; Verifica a melhor pontuaca
	JMP.N	mudaPont
	JMP 	fimCont


; ====================================================================================
; ROTINA: Transforma um hexadecimal num decimal (para output)
;		Recebe o numero e o numero de digitos a apresentar
;		Devolve o "numero" em decimal
; ====================================================================================
transDec:	PUSH 	R1			; Guarda os valores dos registos
	PUSH 	R2
	PUSH 	R3
	PUSH 	R4
	MOV 	R1, M[SP+7]		; R1 fica com o numero
	MOV 	R2, M[SP+6]		; R2 fica com o numero de digitos
tDecLoop:	SHL	R3, 4			; Shifta R3 3 bits para a esquerda
	MOV 	R4, 10			; Coloca o ultimo digito em R4
	DIV 	R1, R4
	ADD 	R3, R4			; Coloca R4 em R3
	DEC	R2			; Decrementa o contador
	CMP 	R2, R0			; Se ja tivermos todos os digitos
	BR.NZ 	tDecLoop			; Sai do ciclo
	MOV 	M[SP+7], R3		; Coloca o "numero" na pilha
	POP 	R4			; Coloca os valores originais nos registos
	POP 	R3
	POP 	R2
	POP	R1
	RETN 	1

; ====================================================================================
; ROTINA:	Gera sequencia aleatoria e escreve-a em memoria
; 		Nao recebe nada
;		Nao devolve nada
; ====================================================================================
random:	PUSH	R1			; Guarda os valores dos registos
	PUSH	R2
	PUSH 	R3
	PUSH	R4
	MOV 	M[sequencia], R0
	MOV 	R1, RAND_MASK		; Coloca uma mascara em R1
	MOV 	R4, 4 			; Coloca o contador a quatro
randLoop:	MOV 	R2, M[espera]		; Gera uma sequencia
	CMP 	R2, R0			; Se IA tiver ocorrido
	BR.Z	divisao			; Salta para a divisao
	TEST 	R1, 1
	BR.Z 	roda
	XOR	R1, RAND_MASK
roda:	ROR 	R1, 1
	BR	randLoop

divisao:	SHL 	M[sequencia], 3		; Shifta a sequencia 3 bits para a esquerda
	MOV 	R2, R1			; Coloca em R2 os ultimos
	AND 	R2, Fh			; 4 bits de R1
	MOV 	R3, 6			; Divide por 6 para obter um resultado
	DIV 	R2, R3			; entre 0 e 5 e incrementa para
	INC 	R3			; ficar com um resultado entre 1 e 6
	ADD 	M[sequencia], R3		; Adiciona a sequencia
	DEC 	R4			; Decrementa o contador
	SHR 	R1, 4			; Shifta R1 4 bits para a direita
	CMP 	R4, R0			; Se R1 for 0
	BR.NZ 	divisao			; Sai do ciclo
	POP 	R4			; Coloca os valores originais nos registos
	POP 	R3
	POP 	R2
	POP	R1
	RET

; ====================================================================================
; ROTINA: Limpa o ecra
;		Nao recebe nada
;		Nao devolve nada
; ====================================================================================
clrScrn:	PUSH 	R1			; Guarda os valores dos registos
	PUSH	R2
	PUSH 	R3
	MOV 	R1, CHR_CLEAR		; R1 fica com espacos
	MOV 	R2, R0			; Coloca o "cursor" a zero
	MOV 	R3, R0			; Coloca R3 a zero
	MOV 	M[CUR_CTRL], R2		; Atualiza a posicao do cursor
	MOV 	M[ESCR_ADR], R1		; Coloca o caracter no ecra
clrLoop:	INC 	R2			; Incrementa o cursor
	MVBL 	R3, R2			; R3 fica com o octeto menos significativo de R2
	CMP 	R3, 80			; Se tiver atingido a coluna 80
	PUSH 	R2			; Muda de linha
	CALL.Z 	newline
	POP 	R2
	MOV 	M[CUR_CTRL], R2		; Atualiza a posicao do cursor
	MOV 	M[ESCR_ADR], R1		; Coloca o caracter no ecra
	CMP	R2, 1800h			; Se chegar a linha 24
	BR.NZ	clrLoop			; Sai do loop
	MOV 	M[cur_texto], R0		; Coloca o cursor na posicao zero
	POP 	R3			; Coloca os valores originais nos registos
	POP 	R2
	POP 	R1
	RET

; ====================================================================================
; ROTINA: Limpa o ecra do LCD
; 		Nao recebe nada
; 		Nao devolve nada
; ====================================================================================
clrLCD:	PUSH	R1			; Guarda o valor de R1
	MOV 	R1, LCD_CLEAR		; R1 fica com o valor para limper o LCD
	MOV 	M[LCD_CONTR], R1		; Envia R1 para o controlo do LCD
	POP 	R1
	RET

; ====================================================================================
; ROTINA:	Compara a sequencia com a tentativa
;		Recebe a sequencia e a tentativa
;		Nao devolve nada
; ====================================================================================
compara:	PUSH 	R1			; Guarda os valores dos registos
	PUSH	R2
	PUSH	R3
	PUSH 	R4
	PUSH 	R5
	PUSH 	R6
	PUSH 	R7
	MOV 	R1, nada			; Interrupcoes desativadas
	MOV 	M[FE01h], R1
	MOV 	M[FE02h], R1
	MOV 	M[FE04h], R1
	MOV 	M[FE05h], R1
	MOV 	M[FE06h], R1
	MOV 	M[FE0Fh], R1
	MOV 	M[TMP_CTRL], R0
	MOV 	R1, M[cur_texto]		; Coloca o cursor na posicao correta
	ADD 	R1, 8
	INC	M[pontuacao]		; Incrementa a pontuacao
	PUSH 	M[pontuacao]
	PUSH 	R1
	CALL 	prtNum			; Escreve o numero da jogada
	POP 	R1
	ADD 	R1, 24			; Coloca o cursor na posicao correta
	PUSH 	M[SP+10]			; Envia a tentativa para a funcao
	PUSH 	R1			; Envia o cursor
	CALL 	prtSeq
	POP 	R1
	ADD 	R1, 26			; Coloca o cursor na posicao correta
	MOV 	M[cur_texto], R1
	MOV 	R7, 4 			; Coloca o contador de semelhancas a quatro
	MOV 	R1, M[SP+9]		; R1 fica com a sequencia
	MOV 	R2, M[SP+10]		; R2 fica com a tentativa
	CMP 	R2, R1			; Verifica se sao iguais
	JMP.Z	correta			; Se forem salta para "correta"
cmpXloop:	MOV 	R3, R1
	MOV 	R4, R2
	AND 	R3, 0111b			; R3 fica com o ultimo digito da sequencia
	AND 	R4, 0111b			; R4 fica com o ultimo digito da tentativa
	CMP 	R3, R0			; Se R3 for 0
	BR.Z	comp_O			; Vai verificar numeros certos nas posicoes erradas
	CMP	R3, R4			; Se forem iguais
	JMP.Z	colocaX			; Coloca X na semelhanca
cmpContX:	ROR 	R1, 3			; Roda R1 3 bits para a direita
	ROR 	R2, 3			; Roda R2 3 bits para a direita
	BR	cmpXloop
comp_O:	MOV 	R5, R0			; Coloca o contador de rotacoes de R1 a zero
	MOV 	R6, 1			; Coloca o contador de rotacoes de R2 a zero
	ROR 	R1, 4			; Roda R1 4 bits para a direita
	ROR 	R2, 7			; Roda R2 7 bits (4+ 3 bits ja verificados)
colOCont:	MOV 	R3, R1
	AND 	R3, 0111b			; R3 fica com os 3 ultimos bits de R1
	CMP 	R3, VOID_SEQ		; Se R3 ja tiver sido analisado
	JMP.Z 	cmpRotR1			; Roda-se R1
cmpOloop:	MOV 	R4, R2
	AND 	R4, 0111b			; R4 fica com os 3 ultimos bits de R2
	CMP 	R6, 4			; Se R6 for 4
	JMP.Z	cmpORot			; Roda R1 e R2
	CMP 	R3, R4			; Se forem iguais
	JMP.Z	colocaO			; Coloca O na semelhanca
cmpContO:	ROR 	R2, 3			; Roda R2 3 bits para a direita
	INC 	R6			; Incrementa o contador de rotacoes de R2
	BR	cmpOloop

comp_fim:	CMP	R7, R0			; Se nao encontrou 4 semelhancas
	JMP.NZ 	colocaT			; Coloca tracos
	PUSH 	M[semelhnca]		; Envia a semelhanca
	PUSH 	M[cur_texto]		; Envia o cursor
	CALL 	output			; Coloca a semelhanca no ecra
	POP 	M[cur_texto]		; Atualiza o cursor
	MOV 	R1, 4			; Faz reset ao numero de interrupcoes
	MOV 	M[n_inter], R1		; que faltam receber
	MOV 	M[semelhnca], R0		; Coloca a semelhanca a zero
	MOV 	M[tentativa], R0 		; Reseta a tentativa
	MOV 	R1, INT1F 		; Ativa as interrupcoes
	MOV 	M[FE01h], R1
	MOV 	R1, INT2F
	MOV 	M[FE02h], R1
	MOV 	R1, INT3F
	MOV 	M[FE03h], R1
	MOV 	R1, INT4F
	MOV 	M[FE04h], R1
	MOV 	R1, INT5F
	MOV 	M[FE05h], R1
	MOV 	R1, INT6F
	MOV 	M[FE06h], R1
	MOV 	R1, INTCF
	MOV 	M[FE0Fh], R1
	MOV 	R1, N100MS		; Inicia o temporizador
	MOV 	M[UNI_CONT], R1
	MOV 	R1, 1
	MOV 	M[TMP_CTRL], R1
	POP 	R7			; Coloca os valores originais nos registos
	POP 	R6
	POP 	R5
	POP 	R4
	POP 	R3
	POP 	R2
	POP 	R1
	RETN 	2

correta:	MOV 	R1, 10101010b		; Coloca a semelhanca em memoria
	MOV 	M[semelhnca], R1
	MOV 	R1, 2			; Coloca a variavel de estado a 2
	MOV 	M[result], R1
	MOV 	R7, R0			; Coloca o contador a zero
	JMP	comp_fim

colocaX:	MOV 	R3, COD_X			; Coloca X na semelhanca
	SHL 	M[semelhnca], 2		; Shifta a semelhanca 2 bits para a esquerda
	ADD 	M[semelhnca], R3		; Coloca na semelhanca
	AND 	R1, FFF8h			; Coloca R1 com os 2 ultimos bits a zero
	OR 	R2, VOID_TENT		; Coloca 111b no final de R2
	DEC 	R7			; Decrementa o contador
	JMP	cmpContX

cmpORot:	ROR 	R2, 4			; Roda R2 4 bits para a direita
	MOV 	R6, R0			; Coloca o contador de rotacoes de R2 a zero
cmpRotR1:	ROR 	R1, 3			; Roda R1 3 bits para a direita
	INC 	R5			; O contador e aumentado
	CMP 	R5, 4			; Se R1 tiver rodado 4 vezes
	JMP.Z	comp_fim			; Sai do loop
	JMP	colOCont

colocaO:	MOV 	R3, COD_O			; Coloca O na semelhanca
	SHL 	M[semelhnca], 2		; Shifta a semelhanca 2 bits para a esquerda
	ADD 	M[semelhnca], R3		; Coloca na semelhanca
	AND 	R1, FFF8h			; Coloca R1 e R2 com os 2 ultimos digitos a zero
	AND 	R3, R0			; Coloca R3 a zero
	OR 	R2, VOID_TENT		; Coloca 111b no final de R2
	DEC 	R7			; Decrementa o contador
	JMP	cmpContO

colocaT:	SHL 	M[semelhnca], 2		; Shifta a semelhanca 2 bits para a esquerda
	DEC 	R7			; Decrementa o contador
	JMP 	comp_fim

; ====================================================================================
; ROTINA:	Escreve as dicas na Janela de Texto
;		Recebe a semelhanca e o cursor
;		Devolve o cursor
; ====================================================================================
output:	PUSH 	R1			; Guarda os valores dos registos
	PUSH 	R2
	PUSH 	R3
	PUSH 	R4
	MOV 	R1, M[SP+7]		; R1 fica com a semelhanca
	MOV 	R2, M[SP+6]		; R2 fica com o cursor
	SHL 	R1, 8			; Shifta R1 8 bits para a esquerda
	MOV 	R4, 4			; Coloca o contador a quatro
outLoop:	MOV 	R3, R1			; Coloca os 2 bits mais significativos de
	AND 	R3, C000h			; R1 em R3
	ROL 	R3, 2			; Roda R3 para colocar o resultado nos 2 ultimos bits
	ADD 	R3, dicas			; Adiciona o endereco das dicas a R3
	MOV 	R3, M[R3]			; R3 fica com o valor armazenado no seu endereco
	MOV 	M[CUR_CTRL], R2		; Atualiza a posicao do cursor
	MOV 	M[ESCR_ADR], R3		; Coloca R3 no ecra
	INC 	R2			; Incrementa o cursor
	DEC 	R4			; Decrementa o contador
	CMP	R4, R0			; Se ja tiverem sido colocados 4 caracteres no ecra
	BR.Z 	outFim			; Sai do ciclo
	SHL 	R1, 2			; Shifta R1 3 bits para a esquerda
	BR 	outLoop

outFim:	PUSH 	R2			; Envia o cursor
	CALL 	newline			; Coloca uma nova linha
	POP	R2			; Atualiza o cursor
	MOV 	M[SP+7], R2		; Coloca o cursor na pilha
	POP	R4			; Coloca os valores originais nos registos
	POP 	R3
	POP 	R2
	POP 	R1
	RETN 	1

; ====================================================================================
; ROTINA: Escrita da pontuacao no LCD
;		Recebe o endereco da string a escrever
;		Nao devolve nada
; ====================================================================================
prntLCD:	PUSH 	R1			; Guarda os valores dos registos
	PUSH	R2
	PUSH 	R3
	PUSH 	R4
	MOV 	R1, M[SP+6]		; R1 fica com o endereco da string
	CALL 	clrLCD			; Limpa o ecra LCD
	MOV 	R2, LCD_ON		; O "cursor" fica a 0 com o LCD ligado
LCDstr:	MOV 	M[LCD_CONTR], R2		; Posiciona o cursor
	MOV 	R3, M[R1]
	CMP 	R3, END_STR		; Se a string chegou ao fim
	BR.Z	LCDsalto			; Sai do ciclo
	MOV 	M[LCD_ESCR],  R3		; Coloca R3 no LCD
	INC 	R2
	INC 	R1
	BR	LCDstr

LCDsalto:	PUSH 	M[mel_pont]		; R1 fica com a melhor pontuacao
	PUSH	2
	CALL 	transDec			; Transforma o numero em decimal
	POP 	R3			; R3 fica com o "numero"
	ADD 	R2, 7			; Centra o cursor
	MOV 	R4, 2			; Coloca o contador a dois
LCDLoop:	MOV 	R1, R3 			; R1 fica com os ultimos
	AND 	R1, Fh			; 4 bits de R3
	ADD 	R1, nums			; Adiciona o endereco de dicas
	MOV 	R1, M[R1]			; R1 fica com a string do seu antigo valor
	MOV 	M[LCD_CONTR], R2		; Atualiza a posicao do cursor
	MOV 	M[LCD_ESCR], R1		; Escreve o numero
	SHR 	R3, 4			; Shifta R3 4 bits para a direita
	INC 	R2			; Incrementa o cursor
	DEC 	R4			; Decrementa o contador
	CMP 	R4, R0			; Se nao houver nenhum digito para colocar
	BR.NZ	LCDLoop 			; Sai do ciclo
	POP 	R4			; Coloca os valores originais nos registos
	POP 	R3
	POP 	R2
	POP 	R1
	RETN	1

; ====================================================================================
; ROTINA: Escrita de strings na Janela de Texto
; 		Recebe o endereco da string e o cursor
;		Devolve o cursor
; ====================================================================================
prntTxt:	PUSH 	R1			; Guarda os valores dos registos
	PUSH 	R2
	PUSH 	R3
	MOV 	R1, M[SP+6]		; R1 fica com o endereco da string
	MOV 	R2, M[SP+5]		; R2 fica com o cursor
prntLoop:	MOV 	M[CUR_CTRL], R2		; Atualiza a posicao do cursor
	MOV 	R3, M[R1]			; R3 fica com o caracter contido em R1
	CMP	R3, END_STR		; Se for o caracter de fim de string
	BR.Z	prntFim			; Sai do ciclo
	CMP 	R3, NEWLINE		; Se for o caracter de nova linha
	BR.Z 	prtNewLn			; Vai para a rotina "newline"
	CMP 	R3, CENTER		; Se for o caracter para centrar na horizontal
	BR.Z	prtCtr			; Vai para a rotina "center"
	CMP 	R3, V_CENTER		; Se for o caracter para centrar na vertical
	BR.Z 	prtVCtr			; Vai para a rotina "vrtCtr"
	CMP 	R3, TABUL			; Se for o caracter para fazer tabulacao
	BR.Z 	prtTab			; Vai para a rotina "tabula"
	MOV 	M[ESCR_ADR], R3		; Escreve o caracter no ecra
	INC	R2			; Incrementa o cursor
prntCont:	INC	R1			; Incrementa o endereco
	BR	prntLoop

prntFim:	MOV 	M[SP+6], R2		; Coloca o cursor na pilha
	POP 	R3			; Coloca os valores originais nos registos
	POP	R2
	POP 	R1
	RETN	1

prtNewLn:	PUSH	R2			; Leva para a rotina "newline"
	CALL	newline
	POP 	R2
	BR 	prntCont

prtCtr:	PUSH 	R2			; Leva para a rotina "center"
	PUSH 	R1
	CALL 	center
	POP 	R2
	BR 	prntCont

prtVCtr:	INC 	R1			; Le o inteiro a seguir ao caracter '^'
	PUSH 	R2			; Envia o cursor
	PUSH 	M[R1]			; Envia o numero de linhas a alinhar
	CALL 	vrtCtr			; Leva para a rotina "vrtCtr"
	POP 	R2
	BR 	prntCont

prtTab:	PUSH 	R2			; Leva oara a rotina "tabula"
	CALL 	tabula
	POP 	R2
	BR 	prntCont

; ====================================================================================
; ROTINA: Envia um sequencia para o ecra
; 		Recebe a sequencia e o cursor
; 		Devolve o cursor
; ====================================================================================
prtSeq:	PUSH 	R1			; Guarda os valores dos registos
	PUSH 	R2
	PUSH 	R3
	MOV 	R1, M[SP+6]		; R1 fica com a sequencia
	MOV 	R2, M[SP+5]		; R2 fica com o cursor
	SHL 	R1, 4			; Shifta a sequencia em 4 bits
pSeqLoop:	MOV 	M[CUR_CTRL], R2		; Atualiza a posicao do cursor
	MOV 	R3, R1			; Copia os ultimos 4 bits de R1
	AND 	R3, E000h			; para R3
	CMP 	R3, R0			; Se for 0, ou seja, se chegamos ao fim
	BR.Z	pSeqFim			; Sai do loop
	ROL 	R3, 3			; Roda R3 3 bits para a esquerda
	ADD 	R3, nums			; Adiciona o valor de R3 ao endereco
	MOV 	R3, M[R3]			; Copia-se o valor do endereco de R3 para R3
	MOV 	M[ESCR_ADR], R3		; Escreve o valor de R3 na janela
	INC 	R2			; Incrementa o cursor
	SHL	R1, 3			; Shifta a sequencia 3 bits para a esquerda
	BR 	pSeqLoop

pSeqFim:	MOV 	M[SP+6], R2		; Coloca o cursor na pilha
	POP 	R3			; Coloca os valores originais nos registos
	POP 	R2
	POP 	R1
	RETN 	1

; ====================================================================================
; ROTINA: Coloca numeros (em decimal) na janela de texto
; 		Recebe o numero e o cursor
;		Devolve o cursor
; ====================================================================================
prtNum:	PUSH 	R1			; Guarda os valores dos registos
	PUSH 	R2
	PUSH 	R3
	PUSH 	R4
	PUSH	M[SP+7]			; Envia o numero
	PUSH 	2
	CALL 	transDec			; Transforma o numero em decimal
	POP 	R3			; R3 fica com o "numero"
	MOV 	R1, M[SP+6]		; R1 fica com o cursor
	MOV 	R4, 2			; Coloca o contador a dois
ptNLoop:	MOV 	R2, R3 			; R2 fica com os ultimos
	AND 	R2, Fh			; 4 bits de R3
	ADD 	R2, nums			; Adiciona o endereco de dicas
	MOV 	R2, M[R2]			; R2 fica com a string do seu antigo valor
	MOV 	M[CUR_CTRL], R1		; Atualiza a posicao do cursor
	MOV 	M[ESCR_ADR], R2		; Escreve o numero
	SHR 	R3, 4			; Shifta R3 4 bits para a direita
	INC 	R1			; Incrementa o cursor
	DEC 	R4			; Decrementa o contador
	CMP 	R4, R0			; Se nao houver nenhum digito para colocar
	BR.NZ	ptNLoop 			; Sai do ciclo
	MOV 	M[SP+7], R1		; Coloca o cursor na pilha
	POP 	R4			; Coloca os valores originais nos registos
	POP 	R3
	POP 	R2
	POP 	R1
	RETN 	1

; ====================================================================================
; 			OPERACOES SOBRE STRINGS
; ROTINA: Coloca o cursor da Janela de Texto numa nova linha
;		Recebe o cursor
;		Devolve o cursor
; ====================================================================================
newline:	PUSH	R1			; Guarda o valor de R1
	MOV 	R1, M[SP+3]		; R1 fica com o cursor
	ADD 	R1, 100h			; Avanca uma linha
	AND 	R1, FF00h			; Coloca o cursor na coluna zero
	MOV 	M[SP+3], R1		; Coloca o cursor na pilha
	POP 	R1			; Coloca o valor original em R1
	RET

; ====================================================================================
; ROTINA: Adiciona ao cursor o valor do parametro de entrada
;		Recebe o cursor
;		Devolve o cursor
; ====================================================================================
tabula:	PUSH 	R1			; Guarda o valor de R1
	MOV 	R1, M[SP+3]		; R1 fica com o cursor
	ADD 	R1, NCHRTAB		; Faz a soma
	MOV 	M[SP+3], R1		; Devolve o cursor
	POP 	R1			; Coloca o valor original em R1
	RET

; ====================================================================================
; ROTINA: Centra a string horizontalmente
;		Recebe o cursor e o endereco da string
;		Devolve o cursor
; ====================================================================================
center:	PUSH 	R1			; Guarda os valores dos registos
	PUSH 	R2
	PUSH 	R3
	MOV 	R1, M[SP+5]		; R1 fica com o endereco da string
	MOV 	R2, R0			; R2 e colocado a -1
	DEC 	R2			; por ser um contador
ctrLoop:	INC 	R2			; Incrementa o contador
	INC 	R1			; Incrementa o endereco
	MOV 	R3, M[R1]
	CMP 	R3, TABUL			; Se e uma tabulacao
	BR.Z	ctrTab			; Salta para "ctrTab"
	CMP 	R3, END_STR		; Verifica se e um caracter de paragem
	BR.Z 	ctrFim
	CMP 	R3, NEWLINE
	BR.Z 	ctrFim
	BR 	ctrLoop

ctrTab:	ADD 	R2, NCHRTAB		; Adiciona o numero de caracteres representados
	BR 	ctrLoop			; pela tabulacao

ctrFim:	MOV 	R1, N_COLUMNS
	SUB 	R1, R2			; Ve o numero de colunas restantes
	MOV 	R2, 2
	DIV 	R1, R2			; Divide por 2
	MOV 	R2, M[SP+6]		; R2 fica com o cursor
	MVBL 	R2, R1
	MOV 	M[SP+6], R2		; Coloca o cursor na pilha
	POP 	R3			; Coloca os valores originais nos registos
	POP 	R2
	POP 	R1
	RETN	1

; ====================================================================================
; ROTINA: Centra as strings verticalmente
;		Recebe o cursor e o numero de strings
;		Devolve o cursor
; ====================================================================================
vrtCtr:	PUSH	R1			; Guarda os valores dos registos
	PUSH 	R2
	MOV 	R1, N_LINES		; R1 fica com o numero de linhas
	MOV	R2, M[SP+4]		; R2 fica com o cursor
	SUB 	R1, R2			; Calcula o numero de linhas restantes
	ROR	R1, 8			; Coloca as linhas no octeto mais significativo
	MOV 	R2, 2			; Divide R1 por R2
	DIV 	R1, R2			; Para este ficar na linha exata
	MOV 	R2, M[SP+5]		; Adiciona-se o cursor ao valor obtido anteriormente
	MVBH 	R2, R1			; movendo apenas a parte mais significativa de R1
	MOV 	M[SP+5], R2		; Coloca o cursor na pilha
	POP 	R2			; Coloca os valores originais nos registos
	POP 	R1
	RETN 	1

; ====================================================================================
; 			ROTINAS DE INTERRUPCAO
; ====================================================================================
INT1F:	PUSH 	R1			; Guarda o valor do R1
	CMP 	M[n_inter], R0		; Se nao sao contabilizadas mais interrupcoes
	BR.Z	int1fim			; Sai da interrupcao
	MOV 	R1, M[tentativa]		; R1 fica com a tentativa
	SHL 	R1, 3			; Shifta 3 bits para a esquerda
	ADD 	R1, 1			; Adiciona o valor associado a interrupcao a R1
	MOV 	M[tentativa], R1		; Atualiza a tentativa
	DEC 	M[n_inter]		; Decrementa o numero de interrupcoes a contabilizar
	POP 	R1			; Coloca o valor original em R1
int1fim:	RTI				; IGUAL PARA AS RESTANTES INTERRUPCOES EXCETO IA E IF (TEMPORIZADOR)

INT2F:	PUSH 	R1
	CMP 	M[n_inter], R0
	BR.Z	int2fim
	MOV 	R1, M[tentativa]
	SHL 	R1, 3
	ADD 	R1, 2
	MOV 	M[tentativa], R1
	DEC 	M[n_inter]
	POP 	R1
int2fim:	RTI

INT3F:	PUSH 	R1
	CMP 	M[n_inter], R0
	BR.Z	int3fim
	MOV 	R1, M[tentativa]
	SHL 	R1, 3
	ADD 	R1, 3
	MOV 	M[tentativa], R1
	DEC 	M[n_inter]
	POP 	R1
int3fim:	RTI

INT4F:	PUSH 	R1
	CMP	M[n_inter], R0
	BR.Z	int4fim
	MOV 	R1, M[tentativa]
	SHL 	R1, 3
	ADD 	R1, 4
	MOV 	M[tentativa], R1
	DEC 	M[n_inter]
	POP 	R1
int4fim:	RTI

INT5F:	PUSH 	R1
	CMP	M[n_inter], R0
	BR.Z	int5fim
	MOV 	R1, M[tentativa]
	SHL 	R1, 3
	ADD 	R1, 5
	MOV 	M[tentativa], R1
	DEC 	M[n_inter]
	POP 	R1
int5fim:	RTI

INT6F:	PUSH 	R1
	CMP	M[n_inter], R0
	BR.Z	int6fim
	MOV 	R1, M[tentativa]
	SHL 	R1, 3
	ADD 	R1, 6
	MOV 	M[tentativa], R1
	DEC 	M[n_inter]
	POP 	R1
int6fim:	RTI

INTAF: 	PUSH 	R1			; Guarda o valor de R1
	MOV 	R1, LED_RESET		; R1 fica com os "LEDs todos ligados"
	MOV 	M[leds], R1		; R1 atualiza os LEDs
	DEC 	M[espera]			; Atualiza a variavel de estado espera (sai do "random")
	POP 	R1			; Coloca o valor original em R1
	RTI

INTCF:	PUSH 	R1			; Guarda o valor de R1
	MOV 	R1, N100MS		; Reinicia o temporizador
	MOV 	M[UNI_CONT], R1
	MOV     	R1, 1
	MOV     	M[TMP_CTRL], R1
	SHR 	M[leds], 1		; Shifta os LEDs em 1 unidade para a esquerda
	POP 	R1			; Coloca o valor original em R1
	RTI

nada: 	RTI				; Interrupcao que nao faz nada
